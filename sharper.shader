/*
	シャープ シェーダー by あるる（きのもと 結衣）
	Sharper Shader by @arlez80

	MIT License
*/

shader_type canvas_item;
render_mode unshaded;

uniform float amount = 0.8;

void fragment( )
{
	float neighbor = amount * -1.0;
	float center = amount * 4.0 + 1.0;

	COLOR = vec4(
		(
			texture( SCREEN_TEXTURE, SCREEN_UV + SCREEN_PIXEL_SIZE * vec2( -1.0, 0.0 ) ).rgb * neighbor
		+	texture( SCREEN_TEXTURE, SCREEN_UV + SCREEN_PIXEL_SIZE * vec2( 0.0, -1.0 ) ).rgb * neighbor
		+	texture( SCREEN_TEXTURE, SCREEN_UV ).rgb * center
		+	texture( SCREEN_TEXTURE, SCREEN_UV + SCREEN_PIXEL_SIZE * vec2( 1.0, 0.0 ) ).rgb * neighbor
		+	texture( SCREEN_TEXTURE, SCREEN_UV + SCREEN_PIXEL_SIZE * vec2( 0.0, 1.0 ) ).rgb * neighbor
		)
	,	1.0
	);
}
